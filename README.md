# Logistics-Tightwad
University collab project. Tucked together by [Tomasz](https://github.com/tomblachut) & [Piotr](https://github.com/PiotrChromniak).

## Screenshots
![Interface](https://raw.githubusercontent.com/tomblachut/Logistics-Tightwad/master/doc/screen.png)
