namespace Tightwad
{
    public class InputRow
    {
        public Sequence Sequence { get; set; }
        public int TimeNormal { get; set; }
        public int TimeBest { get; set; }
        public int CostNormal { get; set; }
        public int CostBest { get; set; }

        public override string ToString()
        {
            return $"Sequence: {Sequence}, TimeNormal: {TimeNormal}, TimeBest: {TimeBest}, CostNormal: {CostNormal}, CostBest: {CostBest}";
        }
    }

}
