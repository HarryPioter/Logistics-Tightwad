﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tightwad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<InputRow> _inputRows;
        public double IndirectCostBase { get; set; }
        public double IndirectCostReduction { get; set; }

        private double _headerMargin = 30;
        private double _horizontalMargin = 0;

        public MainWindow()
        {
            InitializeComponent();

            DataObject.AddPastingHandler(InputData, OnPaste);

            _inputRows = new ObservableCollection<InputRow>();
            InputData.ItemsSource = _inputRows;
        }

        private void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            var isText = e.SourceDataObject.GetDataPresent(DataFormats.UnicodeText, true);
            if (!isText) return;

            var text = e.SourceDataObject.GetData(DataFormats.UnicodeText) as string;

            Debug.WriteLine(text);
        }

        private void Sample1_Click(object sender, RoutedEventArgs e)
        {
            _inputRows = new ObservableCollection<InputRow>
            {
                new InputRow {Sequence = new Sequence(1, 2), TimeNormal = 8, TimeBest = 8, CostNormal = 220, CostBest = 280},
                new InputRow {Sequence = new Sequence(1, 4), TimeNormal = 10, TimeBest = 5, CostNormal = 100, CostBest = 150},
                new InputRow {Sequence = new Sequence(2, 3), TimeNormal = 6, TimeBest = 4, CostNormal = 300, CostBest = 400},
                new InputRow {Sequence = new Sequence(3, 6), TimeNormal = 12, TimeBest = 10, CostNormal = 260, CostBest = 300},
                new InputRow {Sequence = new Sequence(4, 5), TimeNormal = 15, TimeBest = 15, CostNormal = 150, CostBest = 150},
                new InputRow {Sequence = new Sequence(5, 6), TimeNormal = 10, TimeBest = 2, CostNormal = 200, CostBest = 360},
            };
            InputData.ItemsSource = _inputRows;

            Calc_Click(sender, e);
        }

        private void Sample2_Click(object sender, RoutedEventArgs e)
        {
            _inputRows = new ObservableCollection<InputRow>
            {
                new InputRow {Sequence = new Sequence(1, 2), TimeNormal = 6, TimeBest = 4, CostNormal = 10, CostBest = 16},
                new InputRow {Sequence = new Sequence(2, 3), TimeNormal = 4, TimeBest = 3, CostNormal = 13, CostBest = 15},
                new InputRow {Sequence = new Sequence(3, 4), TimeNormal = 5, TimeBest = 5, CostNormal = 12, CostBest = 12},
                new InputRow {Sequence = new Sequence(3, 5), TimeNormal = 8, TimeBest = 6, CostNormal = 14, CostBest = 16},
                new InputRow {Sequence = new Sequence(4, 6), TimeNormal = 10, TimeBest = 10, CostNormal = 6, CostBest = 6},
                new InputRow {Sequence = new Sequence(5, 7), TimeNormal = 6, TimeBest = 6, CostNormal = 11, CostBest = 11},
                new InputRow {Sequence = new Sequence(6, 7), TimeNormal = 3, TimeBest = 2, CostNormal = 4, CostBest = 8},
                new InputRow {Sequence = new Sequence(7, 8), TimeNormal = 2, TimeBest = 2, CostNormal = 3, CostBest = 3},
            };
            InputData.ItemsSource = _inputRows;

            Calc_Click(sender, e);
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            _inputRows = new ObservableCollection<InputRow>();
            InputData.ItemsSource = _inputRows;
            Canvas.Children.Clear();
            Output.Text = "";
        }


        private void Calc_Click(object sender, RoutedEventArgs e)
        {
            if (_inputRows.Count == 0) return;

            Debug.WriteLine("Input:");
            foreach (var row in _inputRows)
            {
                Debug.WriteLine(row);
            }
            Debug.WriteLine($"IndirectCostBase: {IndirectCostBase}");
            Debug.WriteLine($"IndirectCostReduction: {IndirectCostReduction}");

            var graph = GraphManager.generateGraph(_inputRows.ToList());
            var tuple = NetworkCompressor.compressNetwork(graph);

            Debug.WriteLine("OK");

            var results = tuple.Item1;
            var totalTime = (tuple.Item2 - tuple.Item3).TotalDays;
            var cost = tuple.Item4;


            var availableWidth = Canvas.Width - 2 *_horizontalMargin;

            var pxPerDay = availableWidth / totalTime;
            Debug.WriteLine("Total " + totalTime);
            Debug.WriteLine("Len " + pxPerDay);

            var sortedResults = _inputRows.AsEnumerable()
                    .Select(row => results.Find(result => new Sequence(result.Activity.From, result.Activity.To) == row.Sequence))
                    .ToList();

            Canvas.Children.Clear();


            Output.Text = $"Koszt przyspieszenia: {cost}";

            Canvas.Children.Add(CreateHorizonalRuler());

            for (var j = 0; j < totalTime; j++)
            {
                {
                    var rectangle = new Rectangle
                    {
                        Width = 1,
                        Height = Canvas.Height,
                        Fill = Brushes.Gainsboro,
                    };
                    rectangle.SetValue(Canvas.TopProperty, 0.0);
                    rectangle.SetValue(Canvas.LeftProperty, pxPerDay * j + _horizontalMargin);
                    Canvas.Children.Add(rectangle);
                }

                var block = new TextBlock
                {
                    Text = (j + 1).ToString(),
                    Width = pxPerDay,
                    TextAlignment = TextAlignment.Center,
                };
                block.SetValue(Canvas.TopProperty, 10.0);
                block.SetValue(Canvas.LeftProperty, pxPerDay * j);

                Canvas.Children.Add(block);
            }

            var i = 0;
            foreach (var result in sortedResults)
            {
                double shift = pxPerDay * result.TimeShift.TotalDays;
                double width = pxPerDay * result.Duration.TotalDays;
                Canvas.Children.Add(CreateRectangle(i, shift, width, result.IsCritical));
                i++;
            }


        }

        private Rectangle CreateRectangle(double row, double shift, double width, bool isCritical)
        {
            var rectangle = new Rectangle
            {
                Width = width,
                Height = 20,
                Fill = isCritical ? Brushes.OrangeRed : Brushes.Teal,
            };
            rectangle.SetValue(Canvas.TopProperty, row * 20 + _headerMargin);
            rectangle.SetValue(Canvas.LeftProperty, shift + _horizontalMargin);
            return rectangle;
        }

        private Rectangle CreateHorizonalRuler()
        {
            var rectangle = new Rectangle
            {
                Width = Canvas.Width,
                Height = 1,
                Fill = Brushes.Gainsboro,
            };
            rectangle.SetValue(Canvas.TopProperty, _headerMargin);
            rectangle.SetValue(Canvas.LeftProperty, 0.0);
            return rectangle;
        }
    }
}
