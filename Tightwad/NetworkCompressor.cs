using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tightwad
{
    class NetworkCompressor
    {
        public static Tuple<List<ActivityResult>, TimeSpan, TimeSpan, double> compressNetwork(Graph graph)
        {
            var paths = Pathfinder.extractAllPaths(graph);
            var criticalPath = getCriticalPathAndRemainder(paths).Item1;
            var pathTimeBefore = getPathDuration(criticalPath.First());

            var succesfulReduction = true;
            while (succesfulReduction)
            {
                var cPathsAndRemainder = getCriticalPathAndRemainder(paths);

                if (cPathsAndRemainder.Item2 == TimeSpan.Zero)
                {
                    tryReducingPathsUntilLockdown(cPathsAndRemainder.Item1);
                    break;
                }
                else
                    succesfulReduction = tryReducingPaths(cPathsAndRemainder.Item1, cPathsAndRemainder.Item2);
            }

            criticalPath = getCriticalPathAndRemainder(paths).Item1;
            var pathTimeAfter = getPathDuration(criticalPath.First());
            var reductionCost = 0.0d;
            foreach (GraphNode node in graph.Nodes)
            {
                if (node.nodeType == NodeType.INTER)
                {
                    var activity = node.Activity;
                    reductionCost += activity.CostSpeedUp;
                }
            }

            var overview = getActivitiesFinalOverview(criticalPath, paths);

            var pathTimeReduction = pathTimeBefore - pathTimeAfter;

            return new Tuple<List<ActivityResult>, TimeSpan, TimeSpan, double>(overview, pathTimeBefore, pathTimeReduction, reductionCost);
        }

        private static bool tryReducingPaths(List<LinkedList<GraphNode>> criticalPaths, TimeSpan globalRemainder)
        {
            while (globalRemainder > TimeSpan.Zero)
            {
                List<bool> isPathReduced = new List<bool>(criticalPaths.Count);
                for (int i = 0; i < criticalPaths.Count; ++i)
                    isPathReduced.Add(false);

                List<GraphNode> bestNodes = new List<GraphNode>(criticalPaths.Count);
                for (int i = 0; i < criticalPaths.Count; ++i)
                {
                    bestNodes.Add(selectBestActivity(criticalPaths[i]));
                }

                foreach (GraphNode node in bestNodes)
                {
                    if (node.nodeType == NodeType.BEGINNING) //Means no activity to reduce ergo locked
                        return false;
                }

                var nodeRemainders =
                    from node in bestNodes
                    select node.Activity.Remainder;
                var minCommonRemainder = nodeRemainders.Min();
                if (minCommonRemainder > globalRemainder)
                    minCommonRemainder = globalRemainder;

                for (int i = 0; i < criticalPaths.Count; ++i)
                {
                    if (!isPathReduced[i])
                        // searching for mutual nodes to mark them as reduced just before reducing mutual node
                    {
                        for (int j = 0; j < criticalPaths.Count; ++j)
                        {
                            if (j != i && criticalPaths[j].Contains(bestNodes[i]))
                                isPathReduced[j] = true;
                        }
                        bestNodes[i].Activity.reduceTimeDuration(minCommonRemainder);
                        isPathReduced[i] = true;
                    }
                }

                globalRemainder -= minCommonRemainder;
            }

            return true;
        }

        private static void tryReducingPathsUntilLockdown(List<LinkedList<GraphNode>> criticalPaths)
        {
            while (true)
            {
                List<bool> isPathReduced = new List<bool>(criticalPaths.Count);
                for (int i = 0; i < criticalPaths.Count; ++i)
                    isPathReduced.Add(false);

                List<GraphNode> bestNodes = new List<GraphNode>(criticalPaths.Count);
                for (int i = 0; i < criticalPaths.Count; ++i)
                {
                    bestNodes.Add(selectBestActivity(criticalPaths[i]));
                }

                foreach (GraphNode node in bestNodes)
                {
                    if (node.nodeType == NodeType.BEGINNING) //Means no activity to reduce ergo - locked
                        return;
                }

                var nodeRemainders =
                    from node in bestNodes
                    select node.Activity.Remainder;
                var minCommonRemainder = nodeRemainders.Min();


                for (int i = 0; i < criticalPaths.Count; ++i)
                {
                    if (!isPathReduced[i])
                    {
                        for (int j = 0; j < criticalPaths.Count; ++j)
                        {
                            if (j != i && criticalPaths[j].Contains(bestNodes[i]))
                                isPathReduced[j] = true;
                        }
                        bestNodes[i].Activity.reduceTimeDuration(minCommonRemainder);
                        isPathReduced[i] = true;
                    }
                }
            }
        }

        private static Tuple<List<LinkedList<GraphNode>>, TimeSpan> getCriticalPathAndRemainder(List<LinkedList<GraphNode>> paths)
        {
            var pathLengths =
                from path in paths
                select getPathDuration(path);

            var max = pathLengths.Max();

            List<LinkedList<GraphNode>> criticalPaths = new List<LinkedList<GraphNode>>();
            List<LinkedList<GraphNode>> pathsShorterThanCriticals = new List<LinkedList<GraphNode>>();

            for (int i = 0; i < pathLengths.Count(); ++i)
            {
                if (pathLengths.ElementAt(i) == max)
                    criticalPaths.Add(paths[i]);
                else
                    pathsShorterThanCriticals.Add(paths[i]);
            }

            var maxReductionTime = TimeSpan.Zero;

            if (criticalPaths.Count < paths.Count)
            {
                var shorterPathLengths =
                    from path in pathsShorterThanCriticals
                    select getPathDuration(path);

                var shortMax = shorterPathLengths.Max();
                maxReductionTime = max - shortMax;
            }

            return new Tuple<List<LinkedList<GraphNode>>, TimeSpan>(criticalPaths, maxReductionTime);
        }

        public static TimeSpan getPathDuration(LinkedList<GraphNode> path)
        {
            var duration = TimeSpan.Zero;
            foreach (GraphNode node in path)
            {
                if (node.nodeType == NodeType.INTER)
                {
                    var activity = node.Activity;
                    duration += activity.TimeBest + activity.Remainder;
                }
            }
            return duration;
        }

        private static GraphNode selectBestActivity(LinkedList<GraphNode> path)
        {
            double lowestGradient = Double.MaxValue;
            var bestNode = path.First.Value;

            foreach (GraphNode node in path)
            {
                if (node.nodeType == NodeType.INTER)
                {
                    var activity = node.Activity;
                    if (!activity.CostGradient.HasValue || activity.Remainder == TimeSpan.Zero)
                        continue;
                    var nodeGradient = activity.CostGradient;
                    if (nodeGradient < lowestGradient)
                    {
                        lowestGradient = nodeGradient.Value;
                        bestNode = node;
                    }
                }
            }

            return bestNode;
        }

        public static List<Tuple<Sequence, TimeSpan>> generateSequenceDurationList(Graph graph)
        {
            List<Tuple<Sequence, TimeSpan>> list = new List<Tuple<Sequence, TimeSpan>>();
            foreach (GraphNode node in graph.Nodes)
            {
                if (node.nodeType == NodeType.INTER)
                {
                    var activity = node.Activity;

                    var seq = new Sequence(activity.From, activity.To);
                    var time = activity.TimeBest + activity.Remainder;
                    list.Add(new Tuple<Sequence, TimeSpan>(seq, time));
                }
            }
            return list;
        }

        public static List<ActivityResult> getActivitiesFinalOverview(List<LinkedList<GraphNode>> criticalPath,
            List<LinkedList<GraphNode>> paths)
        {
            var criticalActivities = getDistinctActivities(criticalPath);
            var allActivities = getDistinctActivities(paths);

            //Each activity has it's corresponding timeshift in second list
            var activDistinct = allActivities.ToList();
            var activShift = new List<TimeSpan>(activDistinct.Count);

            for (int i = 0; i < activDistinct.Count; ++i)
                activShift.Add(TimeSpan.Zero);
            allActivities.Clear();


            foreach (var path in paths)
            {
                var shift = TimeSpan.Zero;
                var currentNode = path.First;
                while (currentNode.Value.nodeType != NodeType.END)
                {
                    var node = currentNode.Value;
                    var activity = node.Activity;

                    if (node.nodeType == NodeType.INTER)
                    {
                        var index = activDistinct.IndexOf(activity);
                        if (activShift[index] < shift)
                            activShift[index] = shift;
                        shift += activity.TimeBest + activity.Remainder;
                    }

                    currentNode = currentNode.Next;
                }
            }


            var activityResults = new List<ActivityResult>(activDistinct.Count);
            for (int i = 0; i < activShift.Count; ++i)
                activityResults.Add(
                    new ActivityResult(activDistinct[i], activShift[i], criticalActivities.Contains(activDistinct[i])));

            return activityResults;
        }

        private static HashSet<Activity> getDistinctActivities(List<LinkedList<GraphNode>> paths)
        {
            var distinctActivities = new HashSet<Activity>();
            foreach (var path in paths)
                foreach (var node in path)
                    if (node.nodeType == NodeType.INTER)
                        distinctActivities.Add(node.Activity);

            return distinctActivities;
        }
    }
}
