﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tightwad
{
    enum NodeType { BEGINNING, INTER, END }

    class GraphNode
    {
        public readonly NodeType nodeType;
        public readonly Activity Activity;
        public List<GraphNode> NextNodes;
        public List<GraphNode> PrevNodes;

        public GraphNode(NodeType nodeType)
        {
            switch (nodeType)
            {
                case NodeType.BEGINNING:
                    this.nodeType = NodeType.BEGINNING;
                    NextNodes = new List<GraphNode>();
                    PrevNodes = null;
                    break;
                default:
                    this.nodeType = NodeType.END;
                    NextNodes = null;
                    PrevNodes = new List<GraphNode>();
                    break;
            }

            Activity = null;
        }

        public GraphNode(Activity activity)
        {
            Activity = activity;
            nodeType = NodeType.INTER;
            NextNodes = new List<GraphNode>();
            PrevNodes = new List<GraphNode>();
        }
    }
}
