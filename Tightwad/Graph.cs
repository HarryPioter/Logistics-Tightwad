﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tightwad
{
    class Graph
    {
        public GraphNode StartNode { get; set; }
        public List<GraphNode> Nodes { get; set; }

        public Graph()
        {
            Nodes = new List<GraphNode>();
        }
    }
}
