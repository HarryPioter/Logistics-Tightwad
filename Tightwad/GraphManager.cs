using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tightwad
{
    class GraphManager
    {

        public static Graph generateGraph(List<InputRow> rows)
        {
            var maxTo = (from row in rows select row.Sequence.To).Max();
            var minFrom = 1;

            var interNodes = getActivitiesAsGraphNodes(rows);

            for(var i = minFrom + 1; i < maxTo; ++i)
            {
                var nodesToBePointed = interNodes
                    .Where(x => x.Activity.From == i)
                    .ToList();

                foreach(var nodeTo in interNodes.Where(x => x.Activity.To == i)
                    .ToList())
                    foreach(var nodeFrom in nodesToBePointed)
                    {
                        nodeTo.NextNodes.Add(nodeFrom);
                        nodeFrom.PrevNodes.Add(nodeTo);
                    }
            }

            GraphNode START = new GraphNode(NodeType.BEGINNING),
                STOP = new GraphNode(NodeType.END);

            foreach (var startNode in interNodes
                .Where(x => x.Activity.From == minFrom)
                .ToList())
            {
                START.NextNodes.Add(startNode);
                startNode.PrevNodes.Add(START);
            }
        
            foreach(var stopNode in interNodes
                .Where(x => x.Activity.To== maxTo)
                .ToList())
            {
                STOP.PrevNodes.Add(stopNode);
                stopNode.NextNodes.Add(STOP);
            }

            var graph = new Graph();
            graph.StartNode = START;
            graph.Nodes = interNodes.ToList();
            return graph;
        }

        private static IEnumerable<GraphNode> getActivitiesAsGraphNodes(List<InputRow> rows)
        {
            return rows.Select(row => new GraphNode(
                new Activity(row.Sequence.From, row.Sequence.To, row.CostBest, row.CostNormal,
                    new TimeSpan(row.TimeBest, 0, 0, 0), new TimeSpan(row.TimeNormal, 0, 0, 0))))
                    .ToList();
        }

    }
}
