﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tightwad
{
    class ActivityResult
    {
        public readonly Activity Activity;
        public readonly TimeSpan TimeShift;
        public readonly bool IsCritical;
        public readonly TimeSpan Duration;

        public ActivityResult(Activity activity, TimeSpan timeShift, bool isCritical)
        {
            Activity = activity;
            TimeShift = timeShift;
            IsCritical = isCritical;
            Duration = activity.TimeBest + activity.Remainder;
        }
    }
}
