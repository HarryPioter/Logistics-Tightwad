﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Tightwad
{
    class Activity
    {
        public readonly int From;
        public readonly int To;

        public readonly double? CostGradient;
        public readonly double CostBest;
        public readonly double CostNormal;
        public double CostSpeedUp { get; set; }

        public readonly TimeSpan TimeBest;
        public readonly TimeSpan TimeNormal;
        public TimeSpan Remainder { get; set; }

        public Sequence Sequence()
        {
            return new Sequence(From, To);
        }

        public Activity(int from, int to, double costLimit, double costNormal, TimeSpan timeLimit, TimeSpan timeNormal)
        {
            From = from;
            To = to;
            CostSpeedUp = 0.0;
            CostBest = costLimit;
            CostNormal = costNormal;
            TimeBest = timeLimit;
            TimeNormal = timeNormal;
            Remainder = timeNormal - timeLimit;
            if (Remainder == TimeSpan.Zero)
                CostGradient = null;
            else
                CostGradient = (CostBest - CostNormal) / (TimeNormal - TimeBest).Days;
        }

        public void reduceTimeDuration(TimeSpan deltaTime)
        {
            if (CostGradient.HasValue & deltaTime <= Remainder)
            {
                CostSpeedUp += CostGradient.Value * deltaTime.Days;
                Remainder -= deltaTime;
            }
        }

    }
}
