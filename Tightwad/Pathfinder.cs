﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tightwad
{
    class Pathfinder // Mass Effect reference xD
    {
        public static List<LinkedList<GraphNode>> extractAllPaths(Graph graph)
        {
            var paths = new List<LinkedList<GraphNode>>();
            var stack = new Stack<LinkedList<GraphNode>>();

            var currentNode = graph.StartNode;
            var temporaryList = new LinkedList<GraphNode>();
            temporaryList.AddFirst(graph.StartNode);

            do
            {
                if(stack.Count > 0)
                {
                    temporaryList = stack.Pop();
                    currentNode = temporaryList.Last.Value;
                }

                paths.Add(extractCompletePath(temporaryList, stack));
            } while (stack.Count > 0);

            return paths;
        }

        private static LinkedList<GraphNode> extractCompletePath(LinkedList<GraphNode> graphList, Stack<LinkedList<GraphNode>> helperStack)
        {
            var currentNode = graphList.Last();

            while(currentNode.nodeType != NodeType.END)
            {
                if (currentNode.NextNodes.Count > 0)
                {
                    for(int i=1;i<currentNode.NextNodes.Count; ++i)
                    {
                        var branchPath = new LinkedList<GraphNode>(graphList);
                        branchPath.AddLast(currentNode.NextNodes[i]);
                        helperStack.Push(branchPath);
                    }

                    currentNode = currentNode.NextNodes.First();
                    graphList.AddLast(currentNode);
                }
            }

            return graphList;
        }
    }
}
