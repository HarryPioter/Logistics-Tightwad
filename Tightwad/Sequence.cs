using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Data;

namespace Tightwad
{
    public class Sequence : IEquatable<Sequence>
    {
        public int From;
        public int To;

        public Sequence(int from, int to)
        {
            From = from;
            To = to;
        }

        public override string ToString()
        {
            return $"{From}-{To}";
        }

        public bool Equals(Sequence other)
        {
            return From == other.From && To == other.To;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Sequence && Equals((Sequence) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (From*397) ^ To;
            }
        }

        public static bool operator ==(Sequence left, Sequence right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Sequence left, Sequence right)
        {
            return !left.Equals(right);
        }
    }

    [ValueConversion(typeof(Sequence), typeof(string))]
    public class SequenceToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var sequence = (Sequence) value;
            return sequence?.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var text = (string) value;
            try
            {
                var strings = text.Split('-');
                return new Sequence(int.Parse(strings[0]), int.Parse(strings[1]));
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
